<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Artisan;
use App\Thread;
use Log;

/**
 * This job will recieve chunk of threads to close.
 *
 */
class CloseThreadsChunkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param Collection
     */
    protected $threadIDs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $threadIDs)
    {
        $this->threadIDs = $threadIDs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('threads')
            ->whereIn('id', $this->threadIDs->toArray())
            ->update(['closed' => true]); 
        
        // Alterantive to close threads one by one in a loop
        // $this->threadIDs->each(function($threadId){
        //     // TODO fire event close-thread for every thread
        // });
    }
}
