<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Jobs\CloseThreadsChunkJob;
use App\Thread;
use Log;

class CloseOldThreads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'threads:close-old-threads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close all threads with last comment older than month.';


    /**
     * The number of threads that will be closed in one chunk.
     *
     * @var int
     */
    protected $chunkSize = 200;
        
    /**
    * Critical date identifying old threads
    *
    * @var Carbon
    */
    protected $dateLimit;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->dateLimit = Carbon::now()->subDays(30);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('threads')
             ->join('replies', 'replies.thread_id', '=', 'threads.id')
             ->where([['replies.created_at', '<', $this->dateLimit]])
             ->groupBy('threads.id')
             ->pluck('threads.id')
             ->chunk($this->chunkSize)
             ->map(function($chunkOfIds) {
                CloseThreadsChunkJob::dispatch($chunkOfIds); 
             });

    }
}
